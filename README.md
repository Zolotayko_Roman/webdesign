# Pravoslavniy Kino
The best christian movie watching site you have ever been on.

## Usage

### Installation

Install the dependencies (gulp, gulp-sass, browser-sync)

```sh
$ npm install
```

### Run

This will watch your sass, html files, compile them and run your dev server at http://localhost:3002/

```sh
$ npm start
```