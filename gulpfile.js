// var gulp = require('gulp'), // Подключаем Gulp
// sass = require('gulp-sass'); // Подключаем Sass пакет
// var browserSync = require('browser-sync').create();

// // Static server
// gulp.task('rocket', function() {
//     browserSync.init({
//         server: {
//             baseDir: "./"
//         }
//     });
// });

// gulp.task('sass', function() { // Создаем таск "sass"
// return gulp.src(['sass/**/*.sass', 'sass/**/*.scss']) // Берем источник
// .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)) // Преобразуем Sass в CSS посредством gulp-sass
// .pipe(gulp.dest('css')) // Выгружаем результата в папку css
// });

// gulp.task('watch', function() {
// gulp.watch(['sass/**/*.sass', 'sass/**/*.scss'], ['sass']); // Наблюдение за sass файлами в папке sass
// });

// gulp.task('default', ['watch']);

const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');

gulp.task('html', function(){
  gulp.src('src/*.html')
      .pipe(gulp.dest('build'));
});

gulp.task('images', function(){
    gulp.src('src/Pictures/*')
      .pipe(gulp.dest('build/css/Pictures'))
})

gulp.task('sass', function(){
  gulp.src('src/sass/*.sass')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('build/css'))
      .pipe(browserSync.stream());
});

gulp.task('serve', ['sass'], function() {
  browserSync.init({
      server: "./build"
  });
  gulp.watch("src/sass/*.scss", ['sass']);
  gulp.watch("src/*.html", ['html']).on('change', browserSync.reload);
});

gulp.task('default', ['html','images', 'serve']);